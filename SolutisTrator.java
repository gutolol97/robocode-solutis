package solutistrator;
import robocode.*;
import java.awt.Color;
// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html 
/**
 * SolutisTrator - a robot by (José Augusto Jr)
 */
public class SolutisTrator extends Robot{
	int turnDirection = 1;
	int dist = 20;
	int turnAmount = 360;
	/**
	 * run: SolutisTrator's default behavior
	 */
	public void run() {
		setBodyColor(Color.black);
		setGunColor(Color.red);
		setRadarColor(Color.red);
		setBulletColor(Color.black);
		
		while(true) {
			turnGunLeft(turnAmount);
			turnGunRight(turnAmount);
		}
	}
	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {

		if (e.getBearing() >= 0) {
			turnDirection = 1;
		} else {
			turnDirection = -1;
		}

		turnRight(e.getBearing());
		ahead(e.getDistance() + 5);
		scan();
	}
	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		turnRight(90);
		ahead(dist);
		dist *= -1;
		scan();
		fire(1);
	}
	/**
	 * onHitRobot: What to do when you hit a bullet
	 */
	public void onHitRobot(HitRobotEvent e){

		if (e.getBearing() >= 0) {
			turnDirection = 1;
		} else {
			turnDirection = -1;
		}
		turnRight(e.getBearing());
		if (e.getEnergy() > 16) {
			fire(3);
		} else if (e.getEnergy() > 10) {
			fire(2);
		} else if (e.getEnergy() > 4) {
			fire(1);
		} else if (e.getEnergy() > 2) {
			fire(.5);
		} else if (e.getEnergy() > .4) {
			fire(.1);
		}
		ahead(40);
	}
}