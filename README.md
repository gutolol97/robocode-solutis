# Desafio Solutis - Robocode

Robô especializado em combates 1 versus 1
Vantagem: Forte em perseguições e eliminações rápidas
Desvantagem: Dificuldade de lidar com adversários com muita movimentação constante

## onScannedRobot

O que fazer quando ele vê outro robô

Procura o inimigo e quando encontra, anda na direção do mesmo, já que o objetivo do robô é perseguir

## onHitByBullet

O que fazer quando ele é atingido por uma bala

O robô se movimenta em 90 unidades para direita e anda para frente tentar desviar e depois atira

## onHitRobot

O que fazer quando ele acerta um tiro

Se mantém na direção do inimigo e atira o máximo de balas de acordo com sua energia
  
## Considerações finais

Achei muito interessante a iniciativa diferente de se trabalhar com um robô, absorvi muitas informações sobre o gitlab que com certeza vão ser muito úteis ao longo da minha carreira, consegui aprender bastante e tenho vontade de aprender muito mais e encarar novos desafios na Solutis.
